// utils/api.ts
import axios from 'axios';
import { User, Account, Transaction } from '../types';

const API_BASE_URL = 'http://localhost:8080'; // Replace with your actual backend URL

export const getUsers = async (): Promise<User[]> => {
    const response = await axios.get<User[]>(`${API_BASE_URL}/users/findAll`);
    return response.data;
};

export const createUser = async (): Promise<void> => {
    await axios.post(`${API_BASE_URL}/users/create`);
    // Optionally handle response or error
};

export const activateUser = async (userId: number): Promise<void> => {
    await axios.post(`${API_BASE_URL}/users/activate/${userId}`);
    // Optionally handle response or error
};

export const deactivateUser = async (userId: number): Promise<void> => {
    await axios.post(`${API_BASE_URL}/users/deactivate/${userId}`);
    // Optionally handle response or error
};

export const createAccount = async (userId: number) => {
    const response = await axios.post<Account>(`${API_BASE_URL}/accounts/create/${userId}`);
    return response.data;
};

export const getAccountsByUserId = async (userId: number) => {
    const response = await axios.get<Account[]>(`${API_BASE_URL}/accounts/findByUserId/${userId}`);
    return response.data;
};

export const getTransactionsByAccountId = async (accountId: number) => {
    const response = await axios.get<Transaction[]>(`${API_BASE_URL}/transactions/findByAccountId/${accountId}`);
    return response.data;
};
