// types.d.ts
export interface User {
    id: number;
    active: boolean;
    // Add other properties as needed
}

export interface Account {
    id: number;
    userId: number;
    // Add other properties as needed
}

export interface Transaction {
    id: number;
    accountId: number;
    // Add other properties as needed
}
