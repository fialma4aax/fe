This is a simple frontend 


## Getting Started
First, compile the project
```bash
npm install
```


then, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
The frontend is set to fetch data from http://localhost:8080

