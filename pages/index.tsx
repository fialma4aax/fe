// pages/index.tsx

import { GetServerSideProps } from 'next';
import { useState } from 'react';
import { User } from '../types';
import { getUsers, createUser, activateUser, deactivateUser } from '../utils/api';
import Link from 'next/link';

interface Props {
    users: User[];
}

const Home: React.FC<Props> = ({ users: initialUsers }) => {
    const [users, setUsers] = useState<User[]>(initialUsers); // Initialize users state with initialUsers
    const [loading, setLoading] = useState(false);

    const handleCreateUser = async () => {
        setLoading(true);
        try {
            await createUser();
            const updatedUsers = await getUsers();
            setUsers(updatedUsers); // Update users state after creating user
        } catch (error) {
            console.error('Error creating user:', error);
        } finally {
            setLoading(false);
        }
    };

    const handleActivate = async (userId: number) => {
        setLoading(true);
        try {
            await activateUser(userId);
            const updatedUsers = await getUsers();
            setUsers(updatedUsers); // Update users state after activating user
        } catch (error) {
            console.error('Error activating user:', error);
        } finally {
            setLoading(false);
        }
    };

    const handleDeactivate = async (userId: number) => {
        setLoading(true);
        try {
            await deactivateUser(userId);
            const updatedUsers = await getUsers();
            setUsers(updatedUsers); // Update users state after deactivating user
        } catch (error) {
            console.error('Error deactivating user:', error);
        } finally {
            setLoading(false);
        }
    };

    return (
        <div className="container">
            <h1 className="my-4">User List</h1>
            <button className="btn btn-primary mb-3" onClick={handleCreateUser} disabled={loading}>
                {loading ? 'Creating...' : 'Create User'}
            </button>
            <ul className="list-group">
                {users.map(user => (
                    <li key={user.id} className="list-group-item">
                        <div className="d-flex justify-content-between align-items-center">
                            <div>
                                <h5 className="mb-1">{user.id}</h5>
                                <p className="mb-1">Email: user.email</p>
                            </div>
                            <div>
                                {user.active ? (
                                    <span className="badge bg-success">Active</span>
                                ) : (
                                    <span className="badge bg-secondary">Inactive</span>
                                )}
                                <Link className="btn btn-info ms-2" href={`/accounts/${user.id}`}>
                                    View Accounts
                                </Link>
                                <button
                                    className="btn btn-success ms-2"
                                    onClick={() => handleActivate(user.id)}
                                    disabled={loading || user.active}
                                >
                                    Activate
                                </button>
                                <button
                                    className="btn btn-warning ms-2"
                                    onClick={() => handleDeactivate(user.id)}
                                    disabled={loading || !user.active}
                                >
                                    Deactivate
                                </button>
                            </div>
                        </div>
                    </li>
                ))}
            </ul>
        </div>
    );
};

export const getServerSideProps: GetServerSideProps<Props> = async () => {
    try {
        const users = await getUsers();
        return { props: { users } };
    } catch (error) {
        console.error('Error fetching users:', error);
        return { props: { users: [] } };
    }
};

export default Home;
