// pages/accounts/[userId].tsx

import { GetServerSideProps } from 'next';
import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { createAccount, getAccountsByUserId } from '../../utils/api';
import { Account } from '../../types';

interface Props {
    accounts: Account[];
}

const AccountsPage: React.FC<Props> = ({ accounts: initialAccounts }) => {
    const router = useRouter();
    const { userId } = router.query as { userId: string };
    const [accounts, setAccounts] = useState<Account[]>(initialAccounts);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setAccounts(initialAccounts);
    }, [initialAccounts]);

    const handleCreateAccount = async () => {
        setLoading(true);
        try {
            await createAccount(Number(userId));
            const updatedAccounts = await getAccountsByUserId(Number(userId));
            setAccounts(updatedAccounts); // Update local state with new account data
        } catch (error) {
            console.error('Error creating account:', error);
        } finally {
            setLoading(false);
        }
    };

    const handleBack = () => {
        router.push('/'); // Navigate back to user list page
    };

    return (
        <div className="container">
            <h1 className="my-4">Accounts for User {userId}</h1>
            <button className="btn btn-primary mb-3" onClick={handleCreateAccount} disabled={loading}>
                {loading ? 'Creating Account...' : 'Create Account'}
            </button>
            <ul className="list-group">
                {accounts.map(account => (
                    <li key={account.id} className="list-group-item">
                        Account ID: {account.id}
                        {/* Add other account details as needed */}
                    </li>
                ))}
            </ul>
            <button className="btn btn-secondary mt-3" onClick={handleBack}>
                Back to User List
            </button>
        </div>
    );
};

export const getServerSideProps: GetServerSideProps<Props> = async ({ params }) => {
    try {
        const userId = params?.userId as string;
        const accounts = await getAccountsByUserId(Number(userId));
        return { props: { accounts } };
    } catch (error) {
        console.error('Error fetching accounts:', error);
        return { props: { accounts: [] } };
    }
};

export default AccountsPage;
